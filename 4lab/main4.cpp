#include <mbed.h>

DigitalOut oled1(LED1);
InterruptIn ibutton1(BUTTON1);
static auto sleep_time = 1000ms;
Ticker toggle_led_ticker;
void led_ticker()
{
  oled1 = !oled1;
}

void pressed()
{
  toggle_led_ticker.detach();
  oled1 = !oled1;
  sleep_time = 20000000ms;
  toggle_led_ticker.attach(&led_ticker, sleep_time);
}

void released()
{
  toggle_led_ticker.detach();
  oled1 = !oled1;
  sleep_time = 200ms;
  toggle_led_ticker.attach(&led_ticker, sleep_time);
}

int main() {
  toggle_led_ticker.attach(&led_ticker, sleep_time);
  ibutton1.rise(&released);
  ibutton1.fall(&pressed);
}