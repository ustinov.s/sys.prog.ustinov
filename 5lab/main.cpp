#include <mbed.h>


DigitalOut oled1(LED1);
InterruptIn ibutton1(BUTTON1);

#define PRINT_FLAG 66
#define FLAG 1
Thread thread1;
Thread thread2;

void print_thread1()
{
  while (ThisThread::flags_get() != PRINT_FLAG) {
    printf("Thread1\n");
    ThisThread::sleep_for(500ms);
  } 
  printf("Bye from Thread1\n");
}

void print_thread2()
{
  while (ThisThread::flags_get() != FLAG) {
    
    printf("Thread2\n");  
    ThisThread::sleep_for(600ms);
  } 
}

void pressed()
{
  thread1.flags_set(PRINT_FLAG);
}

int main() {
  thread1.start(print_thread1);
  thread2.start(print_thread2);
  ibutton1.rise(&pressed);
}